package com.adepranaya;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SB601-43
 */
public class Kalkulator_153040050 {

    public int tambah(int a, int b) {
        return a + b;
    }

    public int kurang(int a, int b) {
        return a - b;
    }

    public int kali(int a, int b) {
        return a * b;
    }

    public int bagi(int a, int b) {
        return a / b;
    }

    public int menu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("======= Kalkulator 153040050 ========");
        System.out.println("1. tambah");
        System.out.println("2. kurang");
        System.out.println("3. kali");
        System.out.println("4. bagi");
        System.out.println("Masukkan pilihan anda :");
        try {

            int pil = sc.nextInt();
            int a;
            int b;
            if (pil > 0 && pil < 5) {
                System.out.println("Masukkan bilangan a");
                a = sc.nextInt();
                System.out.println("Masukkan bilangan b");
                b = sc.nextInt();
                switch (pil) {
                    case 1:
                        return tambah(a, b);
                    case 2:
                        return kurang(a, b);

                    case 3:
                        return kali(a, b);
                    case 4:
                        return bagi(a, b);
                    default:
                        return -1;
                }

            } else {
                return -2;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return -1;
        }
    }

    public static void main(String[] args) {
        Kalkulator_153040050 k = new Kalkulator_153040050();
        String lanjut;
        do {

            int hasil = k.menu();
            if (hasil == -2 || hasil == -1) {
                
            }else{
                System.out.println("hasil : " + hasil);
            }
            System.out.println("Lanjut? y/n");lanjut = new Scanner(System.in).next();
        } while (lanjut.equalsIgnoreCase("y"));

    }
}
