/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.adepranaya.Kalkulator_153040050;

/**
 *
 * @author SB601-43
 */
public class Testing_Kalkulator {
    
    public Testing_Kalkulator() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testFunction() {
         Kalkulator_153040050 k = new Kalkulator_153040050();
         assertEquals("tambah", 4, k.tambah(2,2));
         assertEquals("kurang", 0, k.kurang(2,2));
         assertEquals("kali", 4, k.kali(2,2));
         assertEquals("bagi", 1, k.bagi(2,2));
     }
}
